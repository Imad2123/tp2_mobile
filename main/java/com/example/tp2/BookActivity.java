package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final BookDbHelper db = new BookDbHelper(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final EditText nameBook = (EditText)findViewById(R.id.nameBook);
        final EditText editAuthors = (EditText)findViewById(R.id.editAuthors);
        final EditText editYear = (EditText)findViewById(R.id.editYear);
        final EditText editGenres = (EditText)findViewById(R.id.editGenres);
        final EditText editPublisher = (EditText)findViewById(R.id.editPublisher);
        final Button btn1 = (Button)findViewById(R.id.btn1);

        Intent intent = getIntent();

        if(intent.hasExtra("book")) {
            Bundle bn = intent.getBundleExtra("book");

            final Book book = (Book) intent.getParcelableExtra("book");


            nameBook.setText(book.getTitle());
            editAuthors.setText(book.getAuthors());
            editYear.setText(book.getYear());
            editGenres.setText(book.getGenres());
            editPublisher.setText(book.getPublisher());

            btn1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    book.setTitle(String.valueOf(nameBook.getText()));
                    book.setAuthors(String.valueOf(editAuthors.getText()));
                    book.setYear(String.valueOf(editYear.getText()));
                    book.setGenres(String.valueOf(editGenres.getText()));
                    book.setPublisher(String.valueOf(editPublisher.getText()));
                    db.updateBook(book);


                    //cnl.sauvgarde(n);
                    Toast.makeText(BookActivity.this, "SAUVGARDAGE DES DONNEES", Toast.LENGTH_LONG).show();
                }
            });
        }

        else{
            btn1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    String title = String.valueOf(nameBook.getText());
                    String authohors = String.valueOf(editAuthors.getText());
                    String year = String.valueOf(editYear.getText());
                    String genre = String.valueOf(editGenres.getText());
                    String pub = String.valueOf(editPublisher.getText());

                    if(title.matches("") || authohors.matches("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                        builder.setMessage("un champ n'est pas bien remplis");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }

                    else{
                        Book book = new Book(title,authohors,year,genre,pub);
                        if(!db.addBook(book)){
                            AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                            builder.setMessage("un champ n'est pas bien remplis");
                            builder.setCancelable(true);
                            builder.show();
                            return;
                        }
                        else{

                            Toast.makeText(BookActivity.this, "SAUVGARDAGE DES DONNEES", Toast.LENGTH_LONG).show();
                        }}}
            });
        }
}
}
