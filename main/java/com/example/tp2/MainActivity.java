package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private MenuItem item;
    SimpleCursorAdapter adapter ;
     ListView list;
     BookDbHelper db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        list = (ListView) findViewById(R.id.list);
        db = new BookDbHelper(getApplicationContext());
        // on fait le db.populate() seulement eu premier lancement de l'application, après on la supprime
        //db.populate();

        final Cursor cursor = db.fetchAllBooks();
        adapter = new SimpleCursorAdapter(MainActivity.this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[]{BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        list.setAdapter(adapter);

        final Intent intent = new Intent(this, BookActivity.class);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor cursor = (Cursor) list.getItemAtPosition(position);
                Book book = db.cursorToBook(cursor);
                intent.putExtra("book", book);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ajouter = new Intent(getApplicationContext(), BookActivity.class);
                startActivity(ajouter);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                // TODO Auto-generated method stub

                registerForContextMenu(list);
                return false;
            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Delete"){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor it = (Cursor)list.getItemAtPosition(info.position);
            Book book=db.cursorToBook(it); db.deleteBook(it);
            Cursor cursor = db.fetchAllBooks();
            adapter.changeCursor(cursor);
            adapter.notifyDataSetChanged();

        }
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity :: onResume()");
        Cursor cursor = db.fetchAllBooks();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }

}
